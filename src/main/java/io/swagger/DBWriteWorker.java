package io.swagger;

import io.swagger.model.Activity;
import io.swagger.model.Course;
import io.swagger.repository.ActivityRepository;
import io.swagger.repository.CourseRepository;
import io.swagger.repository.StuCourseRepository;
import io.swagger.service.ChooseServiceDB;
import io.swagger.service.ChooseServiceRedisImpl;
import io.swagger.service.RetryException;
import io.swagger.spec.ActivitySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DBWriteWorker {
    @Autowired
    private StuCourseRepository stuCourseRepository;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    ChooseServiceRedisImpl chooseServiceRedis;
    @Autowired
    ActivityRepository activityRepository;
    @Autowired
    ChooseServiceDB chooseServiceDB;
    @Autowired
    CourseRepository courseRepository;

    static Duration ScanInterval = Duration.ofMinutes(20);

    private static final Logger log = LoggerFactory.getLogger(DBWriteWorker.class);

    static Long itemCount = 0L;
    static Long errCount=  0L;
    static Long reportInterval = 100L;

    static void addItemCount() {
        itemCount++;
        if (itemCount % reportInterval == 0) {
            log.info(String.format("persist %d item", itemCount));
        }
    }


    boolean writeRecord(Long activityId, Long courseId, Set<Long> stuSet) {
        try {
            var current = stuCourseRepository.findAllByActivityIdAndCourseIdAndDeleted(activityId, courseId, (byte) 0)
                    .stream().map(stuCourse -> stuCourse.getStuId()).collect(Collectors.toSet());
            var removed = current.stream().filter(id -> !stuSet.contains(id));
            var add = stuSet.stream().filter(id -> !current.contains(id));
            Long ts = System.currentTimeMillis();
            for (var stuId: removed.collect(Collectors.toSet())) {
                try {
                    chooseServiceDB.setChooseStatus(stuId, activityId, courseId, (byte) 1);
                    addItemCount();
                } catch (RetryException e) {
                    errCount++;
                }
            }
            for (var stuId: add.collect(Collectors.toSet())) {
                try {
                    chooseServiceDB.setChooseStatus(stuId, activityId, courseId, (byte) 0);
                    addItemCount();
                } catch (RetryException e) {
                    errCount++;
                }
            }
        } catch (Exception e) {
            return false;
        }
        chooseServiceRedis.setDirty(activityId, courseId, false);
        return true;
    }


    public void worker() {
        for (;;) {
            itemCount = 0L;
            errCount = 0L;
            try {
                log.info("start db write back scan");
                var condition = new Activity();
                condition.setActive(1);
                var spec = ActivitySpec.NewSpec(condition);
                var activities = activityRepository.findAll(spec.filter());
                var  scanSt = System.currentTimeMillis();
                for (var act : activities) {
                    var pageSize = 20L;
                    var ret = chooseServiceRedis.GetCourseInActivity(act, new Course(), 1L, pageSize);
                    var count = ret.getCount();
                    var pageNo = 1L;
                    while (count > 0) {
                        ret = chooseServiceRedis.GetCourseInActivity(act, new Course(), pageNo, pageSize);
                        for (var course : ret.getCourseList()) {
//                            log.info(String.format("scan course %d", course.getId()));
                            var lockSt = System.currentTimeMillis();
                            if (!chooseServiceRedis.tryLock(act.getId(), course.getId(), 4)) {
                                log.warn("lock get fail, skip");
                                continue;
                            }
                            try {
//                                log.info(String.format("acquire lock in %d ms", System.currentTimeMillis() - lockSt));

                                if (!chooseServiceRedis.GetDirty(act.getId(), course.getId())) {
                                    continue;
                                }
                                long storeSt = System.currentTimeMillis();
                                var entries = (Set<String>) redisTemplate
                                        .boundSetOps(ChooseServiceRedisImpl.getCourseMemberKey(act.getId(), course.getId())).members();
                                writeRecord(act.getId(), course.getId(), entries.stream().map(s -> Long.valueOf(s)).collect(Collectors.toSet()));

//                                log.info(String.format("persist item in %d ms", System.currentTimeMillis() - storeSt));
                            } finally {
                                chooseServiceRedis.unLock(act.getId(), course.getId());
                            }
                        }
                        pageNo++;
                        count -= pageSize;
                    }
                }
                log.info(String.format("persist %d items, fail %d", itemCount, errCount));
                var scanTs = System.currentTimeMillis() - scanSt;
                var next = Math.max(scanTs * 3, ScanInterval.toMillis());
                log.info(String.format("scan finish in %d ms, next scan in %d ms", scanTs, next));
                valid();
                Thread.sleep(next);
            } catch (Exception e) {
                log.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    void valid() {
        var failCnt = 0;
        for (var course: courseRepository.findAll()) {
            var count = (Long) redisTemplate.boundSetOps(ChooseServiceRedisImpl.getCourseMemberKey(4L, course.getId()))
                    .size();
            if (count != null && !count.equals(course.getCurrent().longValue())) {
                log.info(String.format("id=%d want=%d actual=%d", course.getId(), count, course.getCurrent()));
                failCnt++;
            }
        }
        log.info(String.format("valid fin, fail %d", failCnt));
    }

}
