package io.swagger;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

public class KeepAliveFilter extends GenericFilterBean {

    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws ServletException, IOException {
        final HttpServletResponse httpResponse = (HttpServletResponse) response;
        //httpResponse.setHeader("Connection", "close");
        chain.doFilter(request, response);
    }
}