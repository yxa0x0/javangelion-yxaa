package io.swagger.api;

import io.swagger.SpringUtil;
import io.swagger.model.Activity;
import io.swagger.model.Course;
import io.swagger.model.InlineResponse200;
import io.swagger.model.InlineResponse2002;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.repository.ActivityRepository;
import io.swagger.repository.CourseRepository;
import io.swagger.repository.StuCourseRepository;
import io.swagger.service.ChooseService;
import io.swagger.spec.ActivitySpec;
import io.swagger.spec.CourseSpec;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T07:17:27.394Z[GMT]")
@RestController
public class ActivityApiController implements ActivityApi {

    private static final Logger log = LoggerFactory.getLogger(ActivityApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StuCourseRepository stuCourseRepository;

    @Autowired
    private ChooseService chooseService;

    @org.springframework.beans.factory.annotation.Autowired
    public ActivityApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Transactional
    public ResponseEntity<Void> activityDeletePost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Integer> body) {
        activityRepository.deleteByIdIn(body.stream().map((i) -> new Long(i)).collect(Collectors.toList()));
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<InlineResponse200> activityIdClassPageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "aid", required=true, schema=@Schema()) @PathVariable("id") Long id,@Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo,@Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize,@Parameter(in = ParameterIn.DEFAULT, description = "课程筛选条件", schema=@Schema()) @Valid @RequestBody Course body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                var activityOpt = chooseService.GetActiveActivity(id);
                if (!activityOpt.isPresent()) {
                    return new ResponseEntity<InlineResponse200>(HttpStatus.NOT_FOUND);
                }
                var resp = new InlineResponse200();
                var activity = activityOpt.get();
                var result = chooseService.GetCourseInActivity(activity, body, (long) pageNo, (long) pageSize);
                var stream = chooseService.SetCourseSelected(id, result.getCourseList().stream());
                if (body.getSelected() != null) {
                    stream = stream.filter(course -> course.getSelected() == body.getSelected());
                }
                resp.setData(stream.collect(Collectors.toList()));
                resp.setTotal(result.getCount().intValue());
                resp.setSuccess(true);
                resp.setPageSize(pageSize);
                resp.setCurrent(pageNo);
                return new ResponseEntity<InlineResponse200>(resp, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse200>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse200>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Activity> activityIdGet(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                var spec = ActivitySpec.NewSpec(null);
                return activityRepository.findById(Long.valueOf(id)).map(
                        (activity) -> new ResponseEntity<Activity>(spec.SetActivityActive(activity), HttpStatus.OK)).orElseGet(
                        () -> new ResponseEntity<Activity>(HttpStatus.NOT_FOUND));
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Activity>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Activity>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> activityIdPatch(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Activity body) {
        var oneOption = activityRepository.findById(Long.valueOf(id));
        if (!oneOption.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        if (body.getGrade() != null && body.getGrade().equals("")) {
            body.setGrade("");
        }
        var one = oneOption.get();
        SpringUtil.CopyProperties(body, one);
        activityRepository.save(one);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<InlineResponse200> activityIdStudentStuIdPageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "选课区间id", required=true, schema=@Schema()) @PathVariable("id") Integer id,@Parameter(in = ParameterIn.PATH, description = "学生id", required=true, schema=@Schema()) @PathVariable("stu_id") Integer stuId,@Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo,@Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize,@Parameter(in = ParameterIn.DEFAULT, description = "课程筛选条件", schema=@Schema()) @Valid @RequestBody Activity body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<InlineResponse200>(objectMapper.readValue("{\n  \"total\" : 100,\n  \"current\" : 1,\n  \"data\" : [ {\n    \"total\" : 100,\n    \"current\" : 99,\n    \"teacher\" : \"张颖\",\n    \"grade\" : \"18软件\",\n    \"name\" : 6,\n    \"id\" : 0,\n    \"time\" : 1\n  }, {\n    \"total\" : 100,\n    \"current\" : 99,\n    \"teacher\" : \"张颖\",\n    \"grade\" : \"18软件\",\n    \"name\" : 6,\n    \"id\" : 0,\n    \"time\" : 1\n  } ],\n  \"success\" : true,\n  \"pageSize\" : 20\n}", InlineResponse200.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse200>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse200>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<InlineResponse2002> activityListPageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo,@Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize,@Parameter(in = ParameterIn.DEFAULT, description = "活动区间筛选条件", schema=@Schema()) @Valid @RequestBody Activity body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                var spec = ActivitySpec.NewSpec(body);
                InlineResponse2002 resp = new InlineResponse2002();
                var result = activityRepository.findAll(spec.filter(), PageRequest.of(Math.max(pageNo - 1, 0), pageSize, Sort.by("id").descending()));
                resp.setData(result.get().map(activity -> spec.SetActivityActive(activity)).collect(Collectors.toList()));
                resp.setTotal((int) result.getTotalElements());
                resp.setSuccess(true);
                resp.setPageSize(pageSize);
                resp.setCurrent(pageNo);
                //log.info(resp.toString());
                return new ResponseEntity<InlineResponse2002>(resp, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse2002>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse2002>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> activityPost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Activity> body) {
        activityRepository.saveAll(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
