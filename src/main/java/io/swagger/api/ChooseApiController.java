package io.swagger.api;

import io.swagger.SpringUtil;
import io.swagger.model.InlineResponse2001;
import io.swagger.model.InlineResponse2003;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.ChooseService;
import io.swagger.service.RetryException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T07:17:27.394Z[GMT]")
@RestController
public class ChooseApiController implements ChooseApi {

    private static final Logger log = LoggerFactory.getLogger(ChooseApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private ChooseService chooseService;

    @org.springframework.beans.factory.annotation.Autowired
    public ChooseApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> chooseAidCidStuIdOpPost(@Parameter(in = ParameterIn.PATH, description = "选课区间id", required=true, schema=@Schema()) @PathVariable("aid") Integer aid,@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("cid") Integer cid,@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("stu_id") Integer stuId,@Parameter(in = ParameterIn.PATH, description = "set 选课 unset 取消选课", required=true, schema=@Schema()) @PathVariable("op") String op) {
        Long userId = SpringUtil.CurrentUserID();
        var activtyOpt = chooseService.GetActiveActivity(aid.longValue());
        var courseOpt = chooseService.findCourseById(Long.valueOf(cid));
        if (!activtyOpt.isPresent() || !courseOpt.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (userId == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        try {
            if (op.equals("set")) {
                chooseService.ChooseCourse(userId, aid.longValue(), cid.longValue());
                return new ResponseEntity<>(HttpStatus.OK);
            }
            if (op.equals("unset")) {
                chooseService.RmChooseCourse(userId, aid.longValue(), cid.longValue());
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (RetryException e) {
            return new ResponseEntity<Void>(HttpStatus.GONE);
        } catch (Exception e) {
            log.error("error", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<InlineResponse2003> chooseCidStatusGet(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("cid") Integer cid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<InlineResponse2003>(objectMapper.readValue("{\n  \"total\" : 50,\n  \"current\" : 100\n}", InlineResponse2003.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse2003>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse2003>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<InlineResponse2001> chooseActivityAidCourseCid(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("aid") Long aid, @Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("cid") Long cid,
        @Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("pageNo") Long pageNo, @Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("pageSize") Long pageSize) {
        var result = chooseService.GetUserSelectedCourse(aid, cid, pageNo, pageSize);
        var resp = new InlineResponse2001();
        resp.setCurrent(Math.toIntExact(pageNo));
        resp.setPageSize(Math.toIntExact(pageSize));
        resp.setTotal(result.getValue1().intValue());
        resp.setSuccess(true);
        resp.setData(result.getValue0().collect(Collectors.toList()));
        return new ResponseEntity<InlineResponse2001>(resp, HttpStatus.OK);
    }

}
