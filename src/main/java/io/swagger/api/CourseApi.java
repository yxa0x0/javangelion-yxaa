/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.27).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.Course;
import io.swagger.model.InlineResponse200;
import io.swagger.model.InlineResponse4031;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.CookieValue;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T07:17:27.394Z[GMT]")
@Validated
public interface CourseApi {

    @Operation(summary = "删除课程（管理员）", description = "", tags={ "course" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "succ"),
        
        @ApiResponse(responseCode = "400", description = "一般错误"),
        
        @ApiResponse(responseCode = "403", description = "也许是未授权") })
    @RequestMapping(value = "/course/delete",
        consumes = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<Void> courseDeletePost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Integer> body);


    @Operation(summary = "课程详情", description = "", tags={ "course" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "succ", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Course.class))) })
    @RequestMapping(value = "/course/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Course> courseIdGet(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id);


    @Operation(summary = "修改课程详情", description = "", tags={ "course" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "succ"),
        
        @ApiResponse(responseCode = "403", description = "也许是未授权") })
    @RequestMapping(value = "/course/{id}",
        consumes = { "application/json" }, 
        method = RequestMethod.PATCH)
    ResponseEntity<Void> courseIdPatch(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id, @Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Course body);


    @Operation(summary = "查看课程列表", description = "", tags={ "course" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "输出课程列表", content = @Content(mediaType = "application/json", schema = @Schema(implementation = InlineResponse200.class))) })
    @RequestMapping(value = "/course/list/{page_no}/{page_size}",
        produces = { "application/json" }, 
        consumes = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<InlineResponse200> courseListPageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo, @Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize, @Parameter(in = ParameterIn.DEFAULT, description = "课程筛选条件", schema=@Schema()) @Valid @RequestBody Course body);


    @Operation(summary = "导入新课程（管理员）", description = "", tags={ "course" })
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "succ"),
        
        @ApiResponse(responseCode = "403", description = "err", content = @Content(mediaType = "application/json", schema = @Schema(implementation = InlineResponse4031.class))) })
    @RequestMapping(value = "/course",
        produces = { "application/json" }, 
        consumes = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<Void> coursePost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Course> body);

}

