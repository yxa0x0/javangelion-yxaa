package io.swagger.api;

import io.swagger.SpringUtil;
import io.swagger.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.repository.CourseRepository;
import io.swagger.spec.CourseSpec;
import io.swagger.spec.UserSpec;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T07:17:27.394Z[GMT]")
@RestController
public class CourseApiController implements CourseApi {

    private static final Logger log = LoggerFactory.getLogger(CourseApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private CourseRepository courseRepository;

    @org.springframework.beans.factory.annotation.Autowired
    public CourseApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Transactional
    public ResponseEntity<Void> courseDeletePost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Integer> body) {
        courseRepository.deleteByIdIn(body.stream().map((i) -> new Long(i)).collect(Collectors.toList()));
        //log.info(body.toString());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Course> courseIdGet(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return courseRepository.findById(Long.valueOf(id)).map(
                        (course) -> new ResponseEntity<Course>(course, HttpStatus.OK)).orElseGet(
                                () -> new ResponseEntity<Course>(HttpStatus.NOT_FOUND));
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Course>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Course>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> courseIdPatch(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Course body) {
        var oneOption = courseRepository.findById(Long.valueOf(id));
        if (!oneOption.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        var one = oneOption.get();
        SpringUtil.CopyProperties(body, one);
        courseRepository.save(one);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<InlineResponse200> courseListPageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo,@Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize,@Parameter(in = ParameterIn.DEFAULT, description = "课程筛选条件", schema=@Schema()) @Valid @RequestBody Course body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                var spec = CourseSpec.NewUserSpec(body);
                InlineResponse200 resp = new InlineResponse200();
                var result = courseRepository.findAll(spec.filter(), PageRequest.of(Math.max(pageNo - 1, 0), pageSize, Sort.by("id").descending()));
                resp.setData(result.get().collect(Collectors.toList()));
                resp.setTotal((int) result.getTotalElements());
                resp.setSuccess(true);
                resp.setPageSize(pageSize);
                resp.setCurrent(pageNo);
                //log.info(resp.toString());
                return new ResponseEntity<InlineResponse200>(resp, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse200>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse200>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> coursePost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Course> body) {
        courseRepository.saveAll(body.stream().peek(course -> course.setCurrent(0)).collect(Collectors.toList()));
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
