package io.swagger.api;

import io.swagger.SpringUtil;
import io.swagger.model.*;
import io.swagger.repository.StuCourseRepository;
import io.swagger.repository.UserRepository;
import io.swagger.repository.UserSelectedCourse;
import io.swagger.spec.UserSpec;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T07:17:27.394Z[GMT]")
@RestController
public class UserApiController implements UserApi {

    private static final Logger log = LoggerFactory.getLogger(UserApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StuCourseRepository stuCourseRepository;

    // 123456
    private byte[] defaultPwd = Hex.decodeHex("7c4a8d09ca3762af61e59520943dc26494f8941b");

    @org.springframework.beans.factory.annotation.Autowired
    public UserApiController(ObjectMapper objectMapper, HttpServletRequest request) throws DecoderException {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<User> searchInventory(@Parameter(in = ParameterIn.QUERY, description = "" ,schema=@Schema()) @Valid @RequestParam(value = "stu_id", required = false) Integer stuId,@Parameter(in = ParameterIn.QUERY, description = "用sha1加密(160bit)" ,schema=@Schema()) @Valid @RequestParam(value = "pwd", required = false) Resource pwd) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<User>(objectMapper.readValue("{\n  \"stu_id\" : \"2018204610\",\n  \"gender\" : 1,\n  \"grade\" : \"2018级软件\",\n  \"name\" : \"张弟叔\",\n  \"permission\" : 6,\n  \"id\" : 0\n}", User.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<User>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Collection<StuCourseRespEntity>> userIdCoursePageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id, @Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo, @Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize) {
        String accept = request.getHeader("Accept");
        if (id == 0) {
            id = SpringUtil.CurrentUserID().intValue();
        }
        Map<Long, StuCourseRespEntity> result = new HashMap<>();
        for (var item : stuCourseRepository.findUserCourse(Long.valueOf(id))) {
            if (!result.containsKey(item.getActivityId())) {
                result.put(item.getActivityId(), new StuCourseRespEntity(item.getActivityId(), item.getActivityName(), new ArrayList<>()));
            }
            result.get(item.getActivityId()).getCourse().add(item.getCourse());
        }

        return new ResponseEntity<Collection<StuCourseRespEntity>>(result.values(), HttpStatus.OK);
    }

    public ResponseEntity<User> userIdGet(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                var userOpt = userRepository.findById(Long.valueOf(id));
                if (!userOpt.isPresent()) {
                    return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
                }
                return new ResponseEntity<User>(userOpt.get(), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<User>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> userIdPatch(@Parameter(in = ParameterIn.PATH, description = "", required=true, schema=@Schema()) @PathVariable("id") Integer id,@Parameter(in = ParameterIn.DEFAULT, description = "new_pwd不存在即为不修改密码", schema=@Schema()) @Valid @RequestBody UserIdBody body) {
        String accept = request.getHeader("Accept");
        if (id == 0) {
            id = Math.toIntExact(SpringUtil.CurrentUserID());
        }
        var oneOption = userRepository.findById(Long.valueOf(id));
        if (!oneOption.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        var one = oneOption.get();
        SpringUtil.CopyProperties(body.getUser(), one);
        if (body.getNewPwd() != null) {
            try {
                if (!body.getPwd().equals(Hex.encodeHexString(one.getPwd()))) {
                    return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
                }
                one.setPwd(Hex.decodeHex(body.getNewPwd()));
            } catch (DecoderException e) {
                return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
            }
        }
        userRepository.save(one);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    public ResponseEntity<InlineResponse2001> userListPageNoPageSizePost(@Parameter(in = ParameterIn.PATH, description = "第x页", required=true, schema=@Schema()) @PathVariable("page_no") Integer pageNo,@Parameter(in = ParameterIn.PATH, description = "页大小", required=true, schema=@Schema()) @PathVariable("page_size") Integer pageSize,@Parameter(in = ParameterIn.DEFAULT, description = "用户筛选条件", schema=@Schema()) @Valid @RequestBody User body) {
        String accept = request.getHeader("Accept");
//        var auth = SecurityContextHolder.getContext()
//                .getAuthentication();
//        //log.info(auth.getPrincipal().toString());
        if (accept != null && accept.contains("application/json")) {
            try {
                var spec = UserSpec.NewUserSpec(body);
                InlineResponse2001 resp = new InlineResponse2001();
                Page<User> result = userRepository.findAll(spec.filter(), PageRequest.of(Math.max(pageNo - 1, 0), pageSize, Sort.by("id").descending()));
                resp.setData(result.get().collect(Collectors.toList()));
                resp.setTotal((int) result.getTotalElements());
                resp.setSuccess(true);
                resp.setPageSize(pageSize);
                resp.setCurrent(pageNo);
                return new ResponseEntity<InlineResponse2001>(resp, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse2001>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse2001>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> userPost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<User> body) {
        body = body.stream().map(user -> {user.setPwd(defaultPwd); return user;}).collect(Collectors.toList());
        userRepository.saveAll(body);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<Void> userDeletePost(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<Integer> body) {
        userRepository.deleteByIdIn(body.stream().map((i) -> new Long(i)).collect(Collectors.toList()));
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
