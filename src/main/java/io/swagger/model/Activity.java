package io.swagger.model;

import java.io.Serializable;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Generated;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Activity
 */
@Entity
@Validated
@Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T04:24:20.806Z[GMT]")


public class Activity implements Serializable {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name = "id", nullable = false)
  @JsonProperty("id")
  private Long id = null;
  @Basic
  @Column(name = "begin_time", nullable = false)
  @JsonProperty("begin_time")
  private Long beginTime = null;
  @Basic
  @Column(name = "end_time", nullable = false)
  @JsonProperty("end_time")
  private Long endTime = null;
  @Basic
  @Column(name = "grade", nullable = true, length = 40)
  @JsonProperty("grade")
  private String grade = null;
  @Basic
  @Column(name = "name", nullable = true, length = 40)
  @JsonProperty("name")
  private String name = null;
  @Basic
  @Column(name = "choose_limit", nullable = true)
  @JsonProperty("choose_limit")
  private Integer chooseLimit = null;

  public Integer getActive() {
    return active;
  }

  public void setActive(Integer active) {
    this.active = active;
  }

  @JsonProperty("active")
  private Integer active = null;
  @Basic
  @Column(name = "create_time", nullable = false)
  private long createTime;

  public Activity id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")

    public Long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Activity beginTime(Long beginTime) {
    this.beginTime = beginTime;
    return this;
  }

  /**
   * Get beginTime
   * @return beginTime
   **/
  @Schema(description = "")

    public Long getBeginTime() {
    return beginTime;
  }

  public void setBeginTime(Long beginTime) {
    this.beginTime = beginTime;
  }

  public Activity endTime(Long endTime) {
    this.endTime = endTime;
    return this;
  }

  /**
   * Get endTime
   * @return endTime
   **/
  @Schema(description = "")
  
    public Long getEndTime() {
    return endTime;
  }

  public void setEndTime(Long endTime) {
    this.endTime = endTime;
  }

  public Activity grade(String grade) {
    this.grade = grade;
    return this;
  }

  /**
   * 如果为空，则不限制年级随便选
   * @return grade
   **/
  @Schema(example = "18软件", description = "如果为空，则不限制年级随便选")

    public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public Activity name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(example = "18级第一次选课", description = "")

    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Activity chooseLimit(Integer chooseLimit) {
    this.chooseLimit = chooseLimit;
    return this;
  }

  /**
   * 每个人最多选几节课
   * @return chooseLimit
   **/
  @Schema(example = "5", description = "每个人最多选几节课")

    public Integer getChooseLimit() {
    return chooseLimit;
  }

  public void setChooseLimit(Integer chooseLimit) {
    this.chooseLimit = chooseLimit;
  }


  /**
   * 当前是否正在进行或未开始
   * @return active
   **/
  @Schema(example = "true", description = "当前是否正在进行或未开始")

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Activity activity = (Activity) o;
    return Objects.equals(this.id, activity.id) &&
        Objects.equals(this.beginTime, activity.beginTime) &&
        Objects.equals(this.endTime, activity.endTime) &&
        Objects.equals(this.grade, activity.grade) &&
        Objects.equals(this.name, activity.name) &&
        Objects.equals(this.chooseLimit, activity.chooseLimit) &&
        Objects.equals(this.active, activity.active);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, beginTime, endTime, grade, name, chooseLimit, active);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Activity {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    beginTime: ").append(toIndentedString(beginTime)).append("\n");
    sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
    sb.append("    grade: ").append(toIndentedString(grade)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    chooseLimit: ").append(toIndentedString(chooseLimit)).append("\n");
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(long createTime) {
    this.createTime = createTime;
  }
}
