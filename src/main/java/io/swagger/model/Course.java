package io.swagger.model;

import java.io.Serializable;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Generated;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Course
 */
@Entity
@Validated
@Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T04:24:20.806Z[GMT]")


public class Course implements Serializable {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name = "id", nullable = false)
  @JsonProperty("id")
  private Long id = null;
  @Basic
  @Column(name = "name", nullable = false, length = 45)
  @JsonProperty("name")
  private String name = null;
  @Basic
  @Column(name = "time", nullable = true)
  @JsonProperty("time")
  private Integer time = null;
  @Basic
  @Column(name = "total", nullable = false)
  @JsonProperty("total")
  private Integer total = null;
  @Basic
  @Column(name = "current", nullable = false)
  @JsonProperty("select_cnt")
  private Integer current = null;
  @Basic
  @Column(name = "teacher", nullable = true, length = 20)
  @JsonProperty("teacher")
  private String teacher = null;
  @Basic
  @Column(name = "grade", nullable = true, length = 40)
  @JsonProperty("grade")
  private String grade = null;
  @Basic
  @Column(name = "create_time", nullable = false)
  private long createTime;

  public Long getSelected() {
    return selected;
  }

  public void setSelected(Long selected) {
    this.selected = selected;
  }

  @JsonProperty("selected")
  private Long selected;

  public Long getShowAvailable() {
    return showAvailable;
  }

  public void setShowAvailable(Long showAvailable) {
    this.showAvailable = showAvailable;
  }

  @JsonProperty("show_available")
  private Long showAvailable;


  public Course id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")

    public Long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Course name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(description = "")

    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Course time(Integer time) {
    this.time = time;
    return this;
  }

  /**
   * 上课时间：第几节
   * @return time
   **/
  @Schema(description = "上课时间：第几节")

    public Integer getTime() {
    return time;
  }

  public void setTime(Integer time) {
    this.time = time;
  }

  public Course total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * 设计课容量
   * @return total
   **/
  @Schema(example = "100", description = "设计课容量")

    public Integer getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public Course current(Integer current) {
    this.current = current;
    return this;
  }

  /**
   * 当前多少人报
   * @return current
   **/
  @Schema(example = "99", description = "当前多少人报")
  
    public Integer getCurrent() {
    return current;
  }

  public void setCurrent(int current) {
    this.current = current;
  }

  public void setCurrent(Integer current) {
    this.current = current;
  }

  public Course teacher(String teacher) {
    this.teacher = teacher;
    return this;
  }

  /**
   * Get teacher
   * @return teacher
   **/
  @Schema(example = "张颖", description = "")

    public String getTeacher() {
    return teacher;
  }

  public void setTeacher(String teacher) {
    this.teacher = teacher;
  }

  public Course grade(String grade) {
    this.grade = grade;
    return this;
  }

  /**
   * 如果为空，则为任选课
   * @return grade
   **/
  @Schema(example = "18软件", description = "如果为空，则为任选课")

    public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Course course = (Course) o;
    return Objects.equals(this.id, course.id) &&
        Objects.equals(this.name, course.name) &&
        Objects.equals(this.time, course.time) &&
        Objects.equals(this.total, course.total) &&
        Objects.equals(this.current, course.current) &&
        Objects.equals(this.teacher, course.teacher) &&
        Objects.equals(this.grade, course.grade);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, time, total, current, teacher, grade);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Course {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    time: ").append(toIndentedString(time)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    current: ").append(toIndentedString(current)).append("\n");
    sb.append("    teacher: ").append(toIndentedString(teacher)).append("\n");
    sb.append("    grade: ").append(toIndentedString(grade)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(long createTime) {
    this.createTime = createTime;
  }
}
