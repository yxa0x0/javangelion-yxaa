package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

public class GetCourseInActivityRet implements Serializable {
    private static final long serialVersionUID = -5643614897286870758L;

    public List<Course> getCourseList() {
        return CourseList;
    }

    public void setCourseList(List<Course> courseList) {
        CourseList = courseList;
    }

    public Long getCount() {
        return Count;
    }

    public void setCount(Long count) {
        Count = count;
    }

    @JsonProperty("courseList")
    private List<Course> CourseList;
    @JsonProperty("count")
    private Long Count;

    public GetCourseInActivityRet(List<Course> courseList, Long count) {
        CourseList = courseList;
        Count = count;
    }
}
