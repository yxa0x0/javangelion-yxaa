package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.model.Activity;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * InlineResponse2002
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T04:24:20.806Z[GMT]")


public class InlineResponse2002   {
  @JsonProperty("data")
  @Valid
  private List<Activity> data = null;

  @JsonProperty("total")
  private Integer total = null;

  @JsonProperty("success")
  private Boolean success = null;

  @JsonProperty("pageSize")
  private Integer pageSize = null;

  @JsonProperty("current")
  private Integer current = null;

  public InlineResponse2002 data(List<Activity> data) {
    this.data = data;
    return this;
  }

  public InlineResponse2002 addDataItem(Activity dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<Activity>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
   **/
  @Schema(description = "")
      @Valid
    public List<Activity> getData() {
    return data;
  }

  public void setData(List<Activity> data) {
    this.data = data;
  }

  public InlineResponse2002 total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * Get total
   * @return total
   **/
  @Schema(example = "100", description = "")
  
    public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public InlineResponse2002 success(Boolean success) {
    this.success = success;
    return this;
  }

  /**
   * Get success
   * @return success
   **/
  @Schema(example = "true", description = "")
  
    public Boolean isSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public InlineResponse2002 pageSize(Integer pageSize) {
    this.pageSize = pageSize;
    return this;
  }

  /**
   * Get pageSize
   * @return pageSize
   **/
  @Schema(example = "20", description = "")
  
    public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public InlineResponse2002 current(Integer current) {
    this.current = current;
    return this;
  }

  /**
   * Get current
   * @return current
   **/
  @Schema(example = "1", description = "")
  
    public Integer getCurrent() {
    return current;
  }

  public void setCurrent(Integer current) {
    this.current = current;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2002 inlineResponse2002 = (InlineResponse2002) o;
    return Objects.equals(this.data, inlineResponse2002.data) &&
        Objects.equals(this.total, inlineResponse2002.total) &&
        Objects.equals(this.success, inlineResponse2002.success) &&
        Objects.equals(this.pageSize, inlineResponse2002.pageSize) &&
        Objects.equals(this.current, inlineResponse2002.current);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, total, success, pageSize, current);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2002 {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("    pageSize: ").append(toIndentedString(pageSize)).append("\n");
    sb.append("    current: ").append(toIndentedString(current)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
