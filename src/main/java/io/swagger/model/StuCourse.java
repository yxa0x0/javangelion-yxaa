package io.swagger.model;

import javax.persistence.*;

@Entity
@Table(name = "stu_course", schema = "javangelion", catalog = "")
@IdClass(StuCoursePK.class)
public class StuCourse {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "activity_id", nullable = false, length = 45)
    private long activityId;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "course_id", nullable = false)
    private long courseId;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "stu_id", nullable = false)
    private long stuId;
    @Basic
    @Column(name = "create_time", nullable = false)
    private long createTime;
    @Basic
    @Column(name = "deleted", nullable = false)
    private byte deleted;

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public long getStuId() {
        return stuId;
    }

    public void setStuId(long stuId) {
        this.stuId = stuId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StuCourse stuCourse = (StuCourse) o;

        if (courseId != stuCourse.courseId) return false;
        if (stuId != stuCourse.stuId) return false;
        if (createTime != stuCourse.createTime) return false;
        if (stuCourse.getActivityId() != activityId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (activityId ^ (activityId >>> 32));
        result = 31 * result + (int) (courseId ^ (courseId >>> 32));
        result = 31 * result + (int) (stuId ^ (stuId >>> 32));
        result = 31 * result + (int) (createTime ^ (createTime >>> 32));
        return result;
    }

    public byte getDeleted() {
        return deleted;
    }

    public void setDeleted(byte deleted) {
        this.deleted = deleted;
    }
}
