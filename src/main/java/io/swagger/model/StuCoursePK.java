package io.swagger.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class StuCoursePK implements Serializable {
    @Column(name = "activity_id", nullable = false, length = 45)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long activityId;
    @Column(name = "course_id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long courseId;
    @Column(name = "stu_id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long stuId;

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public long getStuId() {
        return stuId;
    }

    public void setStuId(long stuId) {
        this.stuId = stuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StuCoursePK that = (StuCoursePK) o;
        return activityId == that.activityId && courseId == that.courseId && stuId == that.stuId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(activityId, courseId, stuId);
    }
}
