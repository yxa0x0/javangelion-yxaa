package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class StuCourseRespEntity {
    @JsonProperty("id")
    private Long activityId;
    @JsonProperty("name")
    private String activityName;
    @JsonProperty("course")
    private List<Course> course;

    public StuCourseRespEntity(Long activityId, String activityName, List<Course> course) {
        this.activityId = activityId;
        this.activityName = activityName;
        this.course = course;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public List<Course> getCourse() {
        return course;
    }

    public void setCourse(List<Course> course) {
        this.course = course;
    }
}
