package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Generated;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * User
 */
@Entity
@Validated
@Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T04:24:20.806Z[GMT]")


public class User   {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name = "id", nullable = false)
  @JsonProperty("id")
  private Long id = null;
  @Basic
  @Column(name = "stu_id", nullable = false, length = 20)
  @JsonProperty("stu_id")
  private String stuId = null;
  @Basic
  @Column(name = "name", nullable = true, length = 10)
  @JsonProperty("name")
  private String name = null;
  @Basic
  @Column(name = "grade", nullable = false, length = 40)
  @JsonProperty("grade")
  private String grade = null;
  @Basic
  @Column(name = "permission", nullable = false)
  @JsonProperty("permission")
  private Integer permission = null;
  @Basic
  @Column(name = "gender", nullable = false)
  @JsonProperty("gender")
  private Integer gender = null;
  @Basic
  @Column(name = "pwd", nullable = false)
  @JsonIgnore
  private byte[] pwd;
  @Basic
  @Column(name = "create_time", nullable = false)
  private long createTime;

  public User id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")

    public Long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User stuId(String stuId) {
    this.stuId = stuId;
    return this;
  }

  /**
   * Get stuId
   * @return stuId
   **/
  @Schema(example = "2018204610", description = "")
  
    public String getStuId() {
    return stuId;
  }

  public void setStuId(String stuId) {
    this.stuId = stuId;
  }

  public User name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(example = "张弟叔", description = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public User grade(String grade) {
    this.grade = grade;
    return this;
  }

  /**
   * Get grade
   * @return grade
   **/
  @Schema(example = "2018级软件", description = "")
  
    public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public User permission(Integer permission) {
    this.permission = permission;
    return this;
  }

  /**
   * Get permission
   * @return permission
   **/
  @Schema(description = "")
  
    public Integer getPermission() {
    return permission;
  }

  public void setPermission(Integer permission) {
    this.permission = permission;
  }

  public User gender(Integer gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Get gender
   * @return gender
   **/
  @Schema(description = "")

    public Integer getGender() {
    return gender;
  }

  public void setGender(Integer gender) {
    this.gender = gender;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(this.id, user.id) &&
        Objects.equals(this.stuId, user.stuId) &&
        Objects.equals(this.name, user.name) &&
        Objects.equals(this.grade, user.grade) &&
        Objects.equals(this.permission, user.permission) &&
        Objects.equals(this.gender, user.gender);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, stuId, name, grade, permission, gender);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class User {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    stuId: ").append(toIndentedString(stuId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    grade: ").append(toIndentedString(grade)).append("\n");
    sb.append("    permission: ").append(toIndentedString(permission)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public byte[] getPwd() {
    return pwd;
  }

  public void setPwd(byte[] pwd) {
    this.pwd = pwd;
  }

  public long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(long createTime) {
    this.createTime = createTime;
  }
}
