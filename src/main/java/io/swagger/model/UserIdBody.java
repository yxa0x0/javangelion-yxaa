package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.model.User;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserIdBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-13T04:24:20.806Z[GMT]")


public class UserIdBody   {
  @JsonProperty("user")
  private User user = null;

  @JsonProperty("new_pwd")
  private String newPwd = null;


  @Schema(description = "")
  @Valid
  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  @JsonProperty("pwd")
  private String pwd = null;

  public UserIdBody user(User user) {
    this.user = user;
    return this;
  }

  /**
   * Get user
   * @return user
   **/
  @Schema(description = "")
  
    @Valid
    public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public UserIdBody newPwd(String newPwd) {
    this.newPwd = newPwd;
    return this;
  }

  public UserIdBody pwd(String pwd) {
    this.pwd = pwd;
    return this;
  }

  /**
   * Get newPwd
   * @return newPwd
   **/
  @Schema(description = "")
  
    @Valid
    public String getNewPwd() {
    return newPwd;
  }

  public void setNewPwd(String newPwd) {
    this.newPwd = newPwd;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserIdBody userIdBody = (UserIdBody) o;
    return Objects.equals(this.user, userIdBody.user) &&
        Objects.equals(this.newPwd, userIdBody.newPwd) && Objects.equals(this.pwd, userIdBody.pwd);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, newPwd, pwd);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserIdBody {\n");
    
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    newPwd: ").append(toIndentedString(newPwd)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
