package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.model.User;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserStuIdBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-12-11T03:08:02.261Z[GMT]")


public class UserStuIdBody   {
  @JsonProperty("user")
  private User user = null;

  @JsonProperty("new_pwd")
  private Resource newPwd = null;

  public UserStuIdBody user(User user) {
    this.user = user;
    return this;
  }

  /**
   * Get user
   * @return user
   **/
  @Schema(description = "")
  
    @Valid
    public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public UserStuIdBody newPwd(Resource newPwd) {
    this.newPwd = newPwd;
    return this;
  }

  /**
   * Get newPwd
   * @return newPwd
   **/
  @Schema(description = "")
  
    @Valid
    public Resource getNewPwd() {
    return newPwd;
  }

  public void setNewPwd(Resource newPwd) {
    this.newPwd = newPwd;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserStuIdBody userStuIdBody = (UserStuIdBody) o;
    return Objects.equals(this.user, userStuIdBody.user) &&
        Objects.equals(this.newPwd, userStuIdBody.newPwd);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, newPwd);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserStuIdBody {\n");
    
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    newPwd: ").append(toIndentedString(newPwd)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
