package io.swagger.repository;

import io.swagger.model.Activity;
import io.swagger.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long>, JpaSpecificationExecutor<Activity> {
    public Page<Activity> findAll(Specification<Activity> spec, Pageable pageable);
    public void deleteByIdIn(List<Long> ids);
}
