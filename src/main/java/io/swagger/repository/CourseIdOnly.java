package io.swagger.repository;

public interface CourseIdOnly {
    Long getCourseId();
}