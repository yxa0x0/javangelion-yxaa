package io.swagger.repository;

import io.swagger.model.Course;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@CacheConfig(cacheNames = "course")
public interface CourseRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor<Course> {
    public Page<Course> findAll(Specification<Course> spec, Pageable pageable);
    public void deleteByIdIn(List<Long> ids);
    @Query(value = "select * from course where id=?1 for update", nativeQuery = true)
    public Optional<Course> findCourseWithLock(Long courseId);

    // return affect rows
    @Modifying
    @Query(value = "update course set current =?3 where id=?1 and current = ?2 limit 1", nativeQuery = true)
    public int SetCurrentCount(Long courseId, Long oldVal, Long newVal);

//    @Cacheable(key = "'activityCourse_' + #pageable.pageNumber + '_' + #pageable.pageSize")
//    public Page<Course> findAll(Pageable pageable);

    @Cacheable(key = "'activity' + #activityId + 'Course'")
    //go fuck jpa!!!
    @Query(value = "select c1.* from course c1, course c2 where c1.grade=:grade and c1.id = c2.id and c1.current < c2.total and (1=1 or c1.id=:aid)", nativeQuery = true)
    public List<Course> findAvailableByGrade(@Param("aid") Long activityId, @Param("grade") String grade);
    @Cacheable(key = "'activity' + #activityId + 'Course'")
    @Query(value = "select c1.* from course c1, course c2 where c1.id = c2.id and c1.current < c2.total", nativeQuery = true)
    public List<Course> findAll(Long activityId);
}
