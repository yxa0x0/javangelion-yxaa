package io.swagger.repository;

import io.swagger.model.Activity;
import io.swagger.model.Course;
import io.swagger.model.StuCourse;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.Collection;
import java.util.List;

@Repository
public interface StuCourseRepository  extends JpaRepository<StuCourse, Long>, JpaSpecificationExecutor<StuCourse> {
    Collection<CourseIdOnly> findAllByStuIdAndDeleted(Long stuId, byte deleted);
    List<StuCourse> findAllByCourseIdAndDeleted(Long courseId, byte deleted);
    List<StuCourse> findAllByActivityIdAndCourseIdAndDeleted(Long activityId, Long courseId, byte deleted);
    @Modifying
    @Query(value = "insert into stu_course (stu_id, activity_id, course_id, deleted, create_time)" +
            " values (?1,?2,?3,?4,?5) ON DUPLICATE KEY UPDATE deleted=?4, create_time=?5", nativeQuery = true)
    void SetChooseStatus(Long userId, Long activityId, Long courseId, byte deleted, Long ts);

    @Query(value = "select new io.swagger.repository.UserSelectedCourse(a.id, a.name, c) from Course c, Activity a, StuCourse s" +
            " where s.deleted = 0 and s.activityId = a.id and s.stuId = ?1 and s.courseId = c.id")
    Collection<UserSelectedCourse> findUserCourse(Long userId);
}
