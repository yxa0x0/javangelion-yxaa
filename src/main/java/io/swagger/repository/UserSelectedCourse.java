package io.swagger.repository;

import io.swagger.model.Course;

public class UserSelectedCourse {
    private Long activityId;
    private String activityName;
    private Course course;

    public UserSelectedCourse(Long activityId, String activityName, Course course) {
        this.activityId = activityId;
        this.activityName = activityName;
        this.course = course;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
