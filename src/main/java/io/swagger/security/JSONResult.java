package io.swagger.security;

import org.json.JSONObject;

class JSONResult{
    public static String fillResultString(Integer status, String message, Object result){
        JSONObject jsonObject = new JSONObject(){{
            put("status", status);
            put("uid", message);
            put("result", result);
        }};
        return jsonObject.toString();
    }
}