package io.swagger.service;

import io.swagger.model.Activity;
import io.swagger.model.Course;
import io.swagger.model.GetCourseInActivityRet;
import io.swagger.model.User;
import org.javatuples.Pair;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Stream;

public interface ChooseService {
    // return active activity. return empty if activity not exist or deactivate.
    Optional<Activity> GetActiveActivity(Long activityId);
    Stream<Course> SetCourseSelected(Long activityId, Stream<Course> courseStream);
    GetCourseInActivityRet GetCourseInActivity(Activity activity, Course condition, Long pageNo, Long pageSize);
    void ChooseCourse(Long userId, Long activityId, Long CourseId);
    void RmChooseCourse(Long userId, Long activityId, Long CourseId);
    //boolean UpdateCountLock(Long courseId, Long Delta);
    Optional<Course> findCourseById(Long courseId);
    Pair<Stream<User>, Long> GetUserSelectedCourse(Long activityId, Long courseId, Long pageNo, Long pageSize);
}
