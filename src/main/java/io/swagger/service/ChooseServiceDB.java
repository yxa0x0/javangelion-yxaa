package io.swagger.service;

import io.swagger.repository.ActivityRepository;
import io.swagger.repository.CourseRepository;
import io.swagger.repository.StuCourseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class ChooseServiceDB {
    private static final Logger log = LoggerFactory.getLogger(ChooseServiceDB.class);

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StuCourseRepository stuCourseRepository;

    public boolean UpdateCountLock(Long courseId, Long delta) {
        var courseOpt = courseRepository.findCourseWithLock(courseId);
        if (!courseOpt.isPresent()) {
            return false;
        }
        var course = courseOpt.get();
        var newVal = course.getCurrent() + delta;
        if (newVal < 0 || newVal > course.getTotal()) {
            return false;
        }
        var result = courseRepository.SetCurrentCount(course.getId(), course.getCurrent().longValue(), newVal);
        //log.info(String.valueOf(result));
        return result == 1;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void setChooseStatus(Long userId, Long activityId, Long courseId, byte deleted) {
        if (!UpdateCountLock(courseId, (long) (deleted == 0 ? 1 : -1))) {
            throw new RetryException();
        }
        stuCourseRepository.SetChooseStatus(userId, activityId, courseId, deleted, System.currentTimeMillis());
    }

}
