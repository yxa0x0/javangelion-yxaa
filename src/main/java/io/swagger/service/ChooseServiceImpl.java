package io.swagger.service;

import io.swagger.SpringUtil;
import io.swagger.api.ChooseApiController;
import io.swagger.model.Activity;
import io.swagger.model.Course;
import io.swagger.model.GetCourseInActivityRet;
import io.swagger.model.User;
import io.swagger.repository.ActivityRepository;
import io.swagger.repository.CourseIdOnly;
import io.swagger.repository.CourseRepository;
import io.swagger.repository.StuCourseRepository;
import io.swagger.spec.ActivitySpec;
import io.swagger.spec.CourseSpec;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
abstract public class ChooseServiceImpl implements ChooseService {

    private static final Logger log = LoggerFactory.getLogger(ChooseServiceImpl.class);

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StuCourseRepository stuCourseRepository;

    @Autowired
    private ChooseServiceDB chooseServiceDB;

    @Override
    public Optional<Activity> GetActiveActivity(Long ActivityId) {
        var activitySpec = ActivitySpec.NewSpec(null);
        var activityOpt = activityRepository.findById(ActivityId);
        if (!activityOpt.isPresent()) {
            return Optional.empty();
        }
        var activity = activityOpt.get();
        if (!activitySpec.GetActivityActive(activity)) {
            return Optional.empty();
        }
        return Optional.of(activity);
    }

    @Override
    public Stream<Course> SetCourseSelected(Long activityId, Stream<Course> courseStream) {
        Long currentUser = SpringUtil.CurrentUserID();
        if (currentUser == null) {
            return courseStream;
        }
        var selectedCourse = currentUserSelected(activityId, currentUser).collect(Collectors.toSet());
        courseStream = courseStream.peek(course -> course.setSelected((long) (selectedCourse.contains(course.getId()) ? 1 : 0)));
        return courseStream;
    }

    protected Stream<Long> currentUserSelected(Long activityId, Long currentUser) {
        return stuCourseRepository.findAllByStuIdAndDeleted(currentUser, (byte) 0).stream()
                .map(CourseIdOnly::getCourseId);
    }

    @Override
    public GetCourseInActivityRet GetCourseInActivity(Activity activity, Course condition, Long pageNo, Long pageSize) {
        var courseSpec = CourseSpec.NewUserSpec(condition);
        condition.setGrade(activity.getGrade());
        var result = courseRepository.findAll(courseSpec.Activityfilter(), PageRequest.of(Math.toIntExact(Math.max(pageNo - 1, 0)), Math.toIntExact(pageSize), Sort.by("id").descending()));
        var total = result.getTotalElements();
        var stream = result.stream();

        // set selected property
        return new GetCourseInActivityRet(stream.collect(Collectors.toList()), total);
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void ChooseCourse(Long userId, Long activityId, Long CourseId) {
        chooseServiceDB.setChooseStatus(userId, activityId, CourseId, (byte) 0);
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void RmChooseCourse(Long userId, Long activityId, Long CourseId) {
        chooseServiceDB.setChooseStatus(userId, activityId, CourseId, (byte) 1);
    }

    @Override
    public Optional<Course> findCourseById(Long courseId) {
        return courseRepository.findById(courseId);
    }


}
