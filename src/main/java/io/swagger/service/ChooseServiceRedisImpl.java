package io.swagger.service;

import io.swagger.SpringUtil;
import io.swagger.model.Activity;
import io.swagger.model.Course;
import io.swagger.model.GetCourseInActivityRet;
import io.swagger.model.User;
import io.swagger.repository.CourseRepository;
import io.swagger.repository.StuCourseRepository;
import io.swagger.repository.UserRepository;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@CacheConfig(cacheNames = "c1")
@Primary
public class ChooseServiceRedisImpl extends ChooseServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(ChooseServiceRedisImpl.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StuCourseRepository stuCourseRepository;

    @Autowired
    private UserRepository userRepository;

    private final Duration lockTimeout = Duration.ofMillis(10000);

    @Override
    @Cacheable(key="'activity_' + #ActivityId")
    public Optional<Activity> GetActiveActivity(Long ActivityId) {
        return super.GetActiveActivity(ActivityId);
    }

    @Override
    public Stream<Course> SetCourseSelected(Long activityId, Stream<Course> courseStream) {
        return super.SetCourseSelected(activityId, courseStream).peek(course -> {
            setInitialCourseStatus(activityId, course.getId());
           var count = (Long) redisTemplate.boundSetOps(getCourseMemberKey(activityId, course.getId()))
                   .size();
           course.setCurrent(count.intValue());
        });
    }

    @Override
    public Stream<Long> currentUserSelected(Long activityId, Long currentUser) {
        setInitialUserCourseState(activityId, currentUser);
        return redisTemplate.boundSetOps(getUserCourseSetKey(activityId, currentUser)).members()
                .stream();
    }


    public Pair<Stream<Course>, Long> GetCourseInActivityCache(Activity activity, Course condition, Long pageNo, Long pageSize) {
        var page = courseRepository.findAll(PageRequest.of(Math.toIntExact(pageNo - 1), Math.toIntExact(pageSize)));
        return new Pair<>(page.stream(), page.getTotalElements());
    }

    public boolean matchActivity(Activity activity, Course course) {
        return activity.getGrade() == null || activity.getGrade().equals(course.getGrade());
    }

    public boolean courseAvailable(Course course) {
        return course.getCurrent().equals(course.getTotal());
    }

    @Override
    public GetCourseInActivityRet GetCourseInActivity(Activity activity, Course condition, Long pageNo, Long pageSize) {
        //var spec = new CourseRedisSpec(activity, condition, pageNo, 20L, this);
//        var result = spec.collect(Math.toIntExact(pageSize));
//        return new GetCourseInActivityRet(result, spec.getTotal());
        if (condition.getId() != null) {
            var courseOpt = findCourseById(condition.getId());
            // todo: check show_available
            if (!courseOpt.isPresent() || !matchActivity(activity, courseOpt.get()) ||
                    (condition.getShowAvailable() != null && condition.getShowAvailable() == 1 &&
                            !isCourseAvailable(activity.getId(), courseOpt.get().getId()))) {
                return new GetCourseInActivityRet(new ArrayList<>(), 0L);
            }
            return new GetCourseInActivityRet(
                    SetCourseSelected(activity.getId(), Stream.of(new Course[]{courseOpt.get()})).collect(Collectors.toList()),
                    0L);
        }
        // todo: show_available
        if (condition.getShowAvailable() != null && condition.getShowAvailable() == 1) {
            setInitialCourseAvailable(activity);
            var set = getCourseAvailable(activity.getId());
            var total = set.size();
            var result = set.stream().skip((pageNo - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
            return new GetCourseInActivityRet(SetCourseSelected(activity.getId(),
                    result.stream().map(id -> findCourseById(id).get()))
                    .collect(Collectors.toList()), (long) total);
        }

        // todo: selected
        if (condition.getSelected() != null && condition.getSelected() == 1) {
            var lst = currentUserSelected(activity.getId(), SpringUtil.CurrentUserID()).collect(Collectors.toList());
            var size = lst.size();
            var result = lst.stream().skip((pageNo - 1) * pageSize).limit(pageSize);
            return new GetCourseInActivityRet(SetCourseSelected(activity.getId(),
                   result.map(id -> findCourseById(id).get())).collect(Collectors.toList()), (long) size);
        }

        var page =  courseRepository.findAll(PageRequest.of(Math.toIntExact(pageNo - 1), Math.toIntExact(pageSize)));
        return new GetCourseInActivityRet(SetCourseSelected(activity.getId(), page.stream()).collect(Collectors.toList()),
                page.getTotalElements());
    }

    @Override
    @Cacheable(key = "'course_' + #courseId")
    public Optional<Course> findCourseById(Long courseId) {
        return super.findCourseById(courseId);
    }

    @Override
    public Pair<Stream<User>, Long> GetUserSelectedCourse(Long activityId, Long courseId, Long pageNo, Long pageSize) {
        var stuMember = redisTemplate.boundSetOps(getCourseMemberKey(activityId, courseId));
        var total = stuMember.size();
        return new Pair<Stream<User>, Long>(stuMember.members().stream().skip((pageNo - 1) * pageSize).limit(pageSize)
                .map(id-> {
                    var stuID = ((String) id);
                    var userOpt = userRepository.findById(Long.valueOf(stuID));
                    return userOpt.orElseGet(()->null);
                }).filter(user->user != null), total);
    }

    static public String getCourseLockKey(Long activityId, Long courseId) {
        return String.format("activity%d_course%d_lock", activityId, courseId);
    }

    static public String getUserCourseSetKey(Long activityId, Long userId) {
        return String.format("activity%d_user%d_select", activityId, userId);
    }

    static public String getCourseMemberKey(Long activityId, Long courseId) {
        return String.format("activity%d_course%d_member", activityId, courseId);
    }

    static public String getCourseTotalKey(Long activityId, Long courseId) {
        return String.format("activity%d_course%d_total", activityId, courseId);
    }

    static public String getCourseAvailKey(Long activityId) {
        return String.format("activity%d_course_avail", activityId);
    }

    public boolean getUserCourseSetExist(Long activityId, Long userId) {
        return redisTemplate.hasKey(String.format("activity%d_user%d_exist", activityId, userId));
    }

    public void setUserCourseSetExist(Long activityId, Long userId) {
        redisTemplate.boundValueOps(String.format("activity%d_user%d_exist", activityId, userId))
                .set("dirty");
    }

    public boolean GetDirty(Long activityId, Long courseId) {
        return redisTemplate.hasKey(String.format("activity%d_course%d_dirty", activityId, courseId));
    }

    public void setDirty(Long activityId, Long courseId, boolean dirty) {
        if (dirty) {
            redisTemplate.boundValueOps(String.format("activity%d_course%d_dirty", activityId, courseId))
                    .set("dirty");
        } else {
            redisTemplate.delete(String.format("activity%d_course%d_dirty", activityId, courseId));
        }
    }

    private boolean updateCountLock(Long activityId, Long courseId) {
        return Boolean.TRUE.equals(redisTemplate.opsForValue().setIfAbsent(
                getCourseLockKey(activityId, courseId), "xx", lockTimeout));
    }

    public boolean tryLock(Long activityId, Long courseId, int retry) {
        do {
            if (updateCountLock(activityId, courseId)) {
                return true;
            }
            try {
                Thread.sleep(lockTimeout.toMillis());
            } catch (InterruptedException e) {
                return false;
            }
        } while (retry-- > 0);
        return false;
    }

    public boolean unLock(Long activityId, Long courseId) {
        return redisTemplate.delete(getCourseLockKey(activityId, courseId));
    }

    private void setInitialUserCourseState(Long activityId, Long userId) {
        if (getUserCourseSetExist(activityId, userId)) {
            return;
        }
        stuCourseRepository.findAllByStuIdAndDeleted(userId, (byte) 0).stream()
                .forEach(courseIdOnly -> {
                    redisTemplate.boundSetOps(getUserCourseSetKey(activityId, userId)).add(courseIdOnly.getCourseId());
                });
        setUserCourseSetExist(activityId, userId);
    }

    private Integer setInitialCourseStatus(Long activityId, Long courseId) {
        var total = (Integer) redisTemplate.opsForValue().get(getCourseTotalKey(activityId, courseId));
        if (total != null) {
            return total;
        }
        var course = courseRepository.findById(courseId).get();
        redisTemplate.opsForValue().set(getCourseTotalKey(activityId, courseId), course.getTotal());
        //todo: init course member
        var list = stuCourseRepository.findAllByCourseIdAndDeleted(courseId, (byte) 0);
        for (var record: list) {
            redisTemplate.boundSetOps(getCourseMemberKey(activityId, courseId)).add(Long.valueOf(record.getStuId()).toString());
        }
        return course.getTotal();
    }

    private void setInitialCourseAvailable(Activity activity) {
        if (redisTemplate.hasKey(getCourseAvailKey(activity.getId()))) {
            return;
        }
        Stream<Long> result;
        if (activity.getGrade() == null || activity.getGrade().equals("")) {
            result = courseRepository.findAll(activity.getId()).stream().map(course -> course.getId());
        } else {
            result = courseRepository.findAvailableByGrade(activity.getId(), activity.getGrade()).stream().map(course -> course.getId());
        }

        redisTemplate.boundSetOps(getCourseAvailKey(activity.getId())).add(result.toArray());
    }

    private void pushCourseAvailable(Long activityId, Long courseId) {
        redisTemplate.boundSetOps(getCourseAvailKey(activityId)).add(courseId);
    }

    private void popCourseAvailable(Long activityId, Long courseId) {
        redisTemplate.boundSetOps(getCourseAvailKey(activityId)).remove(courseId);
    }

    private boolean isCourseAvailable(Long activityId, Long courseId) {
        return redisTemplate.boundSetOps(getCourseAvailKey(activityId)).isMember(courseId);
    }

    private Set<Long> getCourseAvailable(Long activityId) {
        return redisTemplate.boundSetOps(getCourseAvailKey(activityId)).members();
    }

    @Override
    public void ChooseCourse(Long userId, Long activityId, Long CourseId) {
        setChooseStatus(userId, activityId, CourseId, (byte) 0);
    }

    @Override
    public void RmChooseCourse(Long userId, Long activityId, Long CourseId) {
        setChooseStatus(userId, activityId, CourseId, (byte) 1);
    }

    protected void setChooseStatus(Long userId, Long activityId, Long courseId, byte deleted) {
        if (!tryLock(activityId, courseId, 5)) {
            throw new RetryException();
        }
        try {
            var delta = deleted == 0 ? 1 : -1;

            var total = (Integer) setInitialCourseStatus(activityId, courseId);

            var stuMember = redisTemplate.boundSetOps(getCourseMemberKey(activityId, courseId));
            var stuKey = userId.toString();
            Long count = stuMember.size();

            if ((stuMember.isMember(stuKey) && deleted == 0) || (!stuMember.isMember(stuKey) && deleted != 0)) {
                throw new RetryException();
            }

            if (count + delta < 0 || count + delta > total) {
                throw new RetryException();
            }

            if (deleted == 0) {
                stuMember.add(stuKey);
                redisTemplate.boundSetOps(getUserCourseSetKey(activityId, userId)).add(courseId);
                if (count + delta == total) {
                    popCourseAvailable(activityId, courseId);
                }
            } else {
                stuMember.remove(stuKey);
                redisTemplate.boundSetOps(getUserCourseSetKey(activityId, userId)).remove(courseId);
                if (count + delta < total) {
                    pushCourseAvailable(activityId, courseId);
                }
            }
            setDirty(activityId, courseId, true);
        } finally {
            unLock(activityId, courseId);
        }
    }

}
