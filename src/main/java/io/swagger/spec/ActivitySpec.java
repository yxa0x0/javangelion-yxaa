package io.swagger.spec;

import io.swagger.model.Activity;
import io.swagger.model.Course;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface ActivitySpec {

    public Specification<Activity> filter();

    public boolean GetActivityActive(Activity activity);
    public Activity SetActivityActive(Activity activity);

    public static ActivitySpec NewSpec(Activity condition) {
        Long ts = System.currentTimeMillis();
        return new ActivitySpec() {
            @Override
            public Specification<Activity> filter() {
                return (root, query, builder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (condition.getName() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("name"),
                                "%" + condition.getName() + "%"));
                    }

                    if (condition.getGrade() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("grade"),
                                condition.getGrade() + "%"));
                    }

                    if (condition.getBeginTime() != null) {
                        var type = root.getModel();
                        predicates.add(builder.greaterThanOrEqualTo(root.get("beginTime"), condition.getBeginTime()));
                    }

                    if (condition.getEndTime() != null) {
                        var type = root.getModel();
                        predicates.add(builder.lessThanOrEqualTo(root.get("beginTime"), condition.getEndTime()));
                    }

                    if (condition.getActive() != null) {
                        var type = root.getModel();
                        if (condition.getActive() == 1) {
                            predicates.add(builder.lessThanOrEqualTo(root.get("beginTime"), ts));
                            predicates.add(builder.greaterThanOrEqualTo(root.get("endTime"), ts));
                        } else {
                            predicates.add(builder.or(new Predicate[]{
                                    builder.greaterThan(root.get("beginTime"), ts),
                                    builder.lessThan(root.get("endTime"), ts),
                            }));
                        }
                    }


                    if (condition.getChooseLimit() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("chooseLimit"), condition.getChooseLimit()));
                    }

                    return builder.and(predicates.toArray(new Predicate[0]));
                };
            }

            @Override
            public boolean GetActivityActive(Activity activity) {
                return ts >= activity.getBeginTime() && ts <= activity.getEndTime();
            }

            @Override
            public Activity SetActivityActive(Activity activity) {
                activity.setActive(GetActivityActive(activity) ? 1 : 0);
                return activity;
            }
        };
    }
}
