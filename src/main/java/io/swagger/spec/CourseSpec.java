package io.swagger.spec;

import io.swagger.model.Course;
import io.swagger.model.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface CourseSpec {

    public Specification<Course> filter();
    public Specification<Course> Activityfilter();

    public static CourseSpec NewUserSpec(Course condition) {
        return new CourseSpec() {
            @Override
            public Specification<Course> filter() {
                return (root, query, builder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (condition.getName() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("name"),
                                "%" + condition.getName() + "%"));
                    }

                    if (condition.getGrade() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("grade"),
                                condition.getGrade() + "%"));
                    }

                    if (condition.getCurrent() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("current"), condition.getCurrent()));
                    }

                    if (condition.getTotal() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("total"), condition.getTotal()));
                    }

                    if (condition.getTeacher() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("teacher"),
                                condition.getTeacher() + "%"));
                    }


                    if (condition.getTime() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("time"), condition.getTime()));
                    }

                    return builder.and(predicates.toArray(new Predicate[0]));
                };
            }

            @Override
            public Specification<Course> Activityfilter() {
                return (root, query, builder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (condition.getName() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("name"),
                                "%" + condition.getName() + "%"));
                    }

                    if (condition.getGrade() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("grade"), condition.getGrade()));
                    }

                    if (condition.getCurrent() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("current"), condition.getCurrent()));
                    }

                    if (condition.getTotal() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("total"), condition.getTotal()));
                    }

                    if (condition.getTeacher() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("teacher"),
                                condition.getTeacher() + "%"));
                    }


                    if (condition.getTime() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("time"), condition.getTime()));
                    }

                    return builder.and(predicates.toArray(new Predicate[0]));
                };
            }
        };
    }
}
