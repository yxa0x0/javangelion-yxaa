package io.swagger.spec;

import io.swagger.model.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface UserSpec {

    public Specification<User> filter();

    public static UserSpec NewUserSpec(User condition) {
        return new UserSpec() {
            @Override
            public Specification<User> filter() {
                return (root, query, builder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (condition.getName() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("name"),
                                "%" + condition.getName() + "%"));
                    }

                    if (condition.getStuId() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("stuId"),
                                condition.getStuId() + "%"));
                    }

                    if (condition.getGrade() != null) {
                        var type = root.getModel();
                        predicates.add(builder.like(
                                root.get("grade"),
                                condition.getGrade() + "%"));
                    }

                    if (condition.getGender() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("gender"), condition.getGender()));
                    }

                    if (condition.getPermission() != null) {
                        var type = root.getModel();
                        predicates.add(builder.equal(root.get("permission"), condition.getPermission()));
                    }

                    return builder.and(predicates.toArray(new Predicate[0]));
                };
            }
        };
    }
}
